import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

import "./qml"
import "./qml/search-table"

ApplicationWindow {
    title: "GL Task 2"

    Material.theme: Material.Dark

    minimumWidth: 1450
    minimumHeight: 640
    
    visible: true

    header: SearchBar {
        leftPadding: 10
    }

    SearchResultsTable {
        anchors.fill: parent
    }

    footer: StatusBar {
        height: 100
    }
}
