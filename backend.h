#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>

#include "file-repository/file_repository.h"

#include "tasks/task.h"

class FileRepository;

class Comparator;

class Backend : public QObject {
	Q_OBJECT
	Q_PROPERTY(FileRepository *repo MEMBER _repository NOTIFY repositoryChanged)
	Q_PROPERTY(QList<QObject *> searchResults READ searchResults NOTIFY searchResultsChanged)
	Q_PROPERTY(QObject *task READ task NOTIFY taskChanged)
	Q_PROPERTY(QString taskType READ taskType NOTIFY taskChanged)
public:
	using QObject::QObject;
	~Backend();

	const QObjectList &searchResults() const { return _searchResults; }
	QObject *task() { return _task; }
	QString taskType() { return _taskType; }
public slots:
	void startIndexingTask();
	void startDeserializingTask();
	void startSearchTask(QString property, QString propertyValue);

signals:
	void repositoryChanged();
	void searchResultsChanged();
	void taskChanged();

private:
	void setRepo(FileRepository *repo);
	void setCurrentTask(Task *task, QString type);

private:
	FileRepository *_repository = nullptr;
	QObjectList _searchResults;
	Task *_task = nullptr;
	QString _taskType = "idle";
};

#endif // BACKEND_H
