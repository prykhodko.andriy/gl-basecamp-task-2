#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>

#include "backend.h"

int main(int argc, char *argv[]) {
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
	QCoreApplication::setOrganizationName("");

	QGuiApplication app(argc, argv);

	QQuickStyle::setStyle("Material");

	QQmlApplicationEngine engine("qrc:/main.qml");

	Backend backend;
	engine.rootContext()->setContextProperty("Backend", &backend);

	return app.exec();
}
