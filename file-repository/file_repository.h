#ifndef FILE_REPO_H
#define FILE_REPO_H

#include <QObject>

#include <QDateTime>
#include <QMultiMap>
#include <QVector>

#include "file_record.h"

using FileRecordsList = QList<FileRecord *>;

template<typename T>
using FileRecordsMap = QMultiMap<T, FileRecord *>;

// Note: we copy arguments like QString, QDateTime because they are COW classes(Copy on write)

class FileRepository : public QObject {
	Q_OBJECT
	Q_PROPERTY(uint count READ count NOTIFY countChanged)
public:
	using QObject::QObject;

	uint count() {
		return _count;
	}

signals:
	void countChanged();

public slots:
	FileRecord *addFileRecord(QString name, QString path, QString ext, QDateTime date, quint64 size) {
		auto record = new FileRecord{name, path, ext, date, size};
		record->setParent(this);

		_nameMap.insert(record->name(), record);
		_extensionMap.insert(record->extension(), record);
		_dateMap.insert(record->date(), record);
		_sizeMap.insert(record->size(), record);

		_count++;
		emit countChanged();
		return record;
	}

	FileRecordsList filesByName(QString name) const { return _nameMap.values(name); }
	FileRecordsList filesByExtension(QString extension) const { return _extensionMap.values(extension); }
	FileRecordsList filesByDate(QDateTime date) const { return _dateMap.values(date); }
	FileRecordsList filesBySize(quint64 size) const { return _sizeMap.values(size); }

private:
	FileRecordsMap<QString> _nameMap;
	FileRecordsMap<QString> _extensionMap;
	FileRecordsMap<QDateTime> _dateMap;
	FileRecordsMap<quint64> _sizeMap;
	quint64 _count = 0;
};

#endif // FILE_REPO_H
