#ifndef FILE_RECORD_H
#define FILE_RECORD_H

#include <QObject>

#include <QDateTime>

class FileRecord : public QObject {
	Q_OBJECT
	Q_PROPERTY(QString name READ name CONSTANT)
	Q_PROPERTY(QString extension READ extension CONSTANT)
	Q_PROPERTY(QString path READ path CONSTANT)
	Q_PROPERTY(QDateTime date READ date CONSTANT)
	Q_PROPERTY(quint64 size READ size CONSTANT)

public:
	FileRecord(QString name, QString ext, QString path, QDateTime dt, quint64 size) {
		_name = name;
		_extension = ext;
		_path = path;
		_date = dt;
		_size = size;
	}

	QString name() const { return _name; }
	QString extension() const { return _extension; }
	QString path() const { return _path; }

	QDateTime date() const { return _date; }

	quint64 size() const { return _size; }

private:
	QString _name;
	QString _extension;
	QString _path;
	QDateTime _date;
	quint64 _size;
};

#endif // FILE_RECORD_H
