import QtQuick 2.12
import QtQuick.Controls 2.12

Item {
    Timer {
        id: timer
        interval: 300
        repeat: false
        running: true
        onTriggered: Backend.startDeserializingTask()
    }

    Column {
        anchors.verticalCenter: parent.verticalCenter
        
        padding: 10
        spacing: 10

        Row {
            spacing: 5
            
            Label {
                text: 'Статус: '
            }

            Label {
                text: {
                    switch (Backend.taskType) {
                    case 'indexing':
                        return 'Индексация'
                    case 'loading':
                        return 'Загрузка индекса'
                    case 'searching':
                        return 'Поиск'
                    default:
                        return 'Ожидание'
                    }
                }
            }
        }

        Row {
            spacing: 5
            
            Label {
                text: 'Текущая индексируемая папка: '
            }

            Label {
                text: Backend.taskType === 'indexing' ? Backend.task.currDir : 'N/A'
            }
        }

        Label {
            text: 'Файлов в индексе: ' + (Backend.taskType === 'indexing' ? Backend.task.filesCount : (Backend.repo ? Backend.repo.count : 'N/A'))
        }
    }    

    Row {
        anchors.right: parent.right
        anchors.rightMargin: 50
        anchors.verticalCenter: parent.verticalCenter

        width: 150

        Button {
            text: {
                if (!Backend.task) {
                    return 'Старт'
                } else if (Backend.task && Backend.task.paused) {
                    return 'Возобновить'
                } 
                return 'Пауза'
            }
            onPressed: {
                if (!Backend.task) {
                    Backend.startIndexingTask()
                } else if (Backend.task.paused) {
                    Backend.task.paused = false
                } else {
                    Backend.task.paused = true
                }
            }
        }

        Button {
            text: 'Стоп'
            enabled: task
            onPressed: {
                Backend.task.cancel()
            }
        }
    }
}       
    