import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.15

Row {
    height: 100
    spacing: 25

    ComboBox {
        id: combo
        width: 120
        anchors.verticalCenter: parent.verticalCenter
        model: ["name", "extension", "size", "date"]
    }

    TextField {
        id: textField
        anchors.verticalCenter: parent.verticalCenter
        width: 300
        inputMethodHints: {
            if (combo.currentText === 'size') {
                return Qt.ImhDigitsOnly
            }
            return Qt.ImhNone
        }
        placeholderText: combo.currentText === 'date' ? 'YYYY-MM-DDTHH:MM:SS' : ''
        validator: combo.currentText === 'date' ? dateValidator : null

        RegularExpressionValidator {
            id: dateValidator
            regularExpression: /[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])T(2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]/
        }
    }

    ComboBox {
        id: szCombo
        anchors.verticalCenter: parent.verticalCenter
        visible: combo.currentText === 'size'
        model: ["B", "KB", "MB", "GB"]
    }

    Button {
        anchors.verticalCenter: parent.verticalCenter
        text: 'Поиск'
        enabled: !Backend.task && Backend.repo && textField.acceptableInput
        onPressed: {
            let val = textField.text
            if (combo.currentText === 'size') {
                val = (+val) * [1, 1024, Math.pow(1024, 2), Math.pow(1024, 3)][szCombo.currentIndex]
            } else if (combo.currentText === 'date') {
                val = +Date.parse(val)
                console.log(val)
            }

            Backend.startSearchTask(combo.currentText, val)
        }
    }

}