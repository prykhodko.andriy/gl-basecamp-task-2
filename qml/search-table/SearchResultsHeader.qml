import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Rectangle {
    color: Material.color(Material.Grey, Material.Shade800)

    anchors.left: parent.left
    anchors.right: parent.right

    height: 50

    Row {
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 10

        spacing: 0

        Label {
            text: 'Имя'
            width: 400
        }
        Label {
            text: 'Тип'
            width: 100
        }
        Label {
            text: 'Дата'
            width: 200
        }
        Label {
            text: 'Размер'
            width: 100
        }
        Label {
            text: 'Путь'
        }
    }
}