import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

ListView {
    id: listView
    header: SearchResultsHeader { z: 2 }
    headerPositioning: ListView.OverlayHeader
    clip: true
    spacing: 1

    model: Backend.searchResults

    delegate: SearchResultDelegate {}
    
    ScrollBar.vertical: ScrollBar {
        active: true
    }
}