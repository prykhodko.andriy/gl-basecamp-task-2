import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Rectangle {
    color: Material.color(Material.Grey, Material.Shade700)

    anchors.left: parent.left
    anchors.right: parent.right

    height: 40

    Row {
        anchors.verticalCenter: parent.verticalCenter
        
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 10

        spacing: 0

        Label {
            anchors.verticalCenter: parent.verticalCenter
            text: modelData.name
            width: 400
        }
        Label {
            anchors.verticalCenter: parent.verticalCenter
            text: modelData.extension
            width: 100
        }
        Label {
            anchors.verticalCenter: parent.verticalCenter
            text: modelData.date
            width: 200
        }
        Label {
            anchors.verticalCenter: parent.verticalCenter
            text: getHumanReadableBytes(modelData.size)
            width: 100
        }
        Label {
            anchors.verticalCenter: parent.verticalCenter
            text: modelData.path
            width: 600
        }

    }
    
    ToolButton {
        anchors.right: parent.right
        icon.source: 'qrc:/icons/folder.svg'
        onPressed: Qt.openUrlExternally("file://" + modelData.path);
    }

    function getHumanReadableBytes(bytes) {
        if (bytes < 1024) {
            return bytes.toString() + 'B'
        }

        let floating = bytes / 1024.0
        let unitsOut = 'KB'

        if (floating >= 1024) {
            floating /= 1024.0
            unitsOut = 'MB'
        }

        if (floating >= 1024) {
            floating /= 1024.0
            unitsOut = 'GB'
        }

        if (floating >= 1024) {
            floating /= 1024.0
            unitsOut = 'TB'
        }

        return floating.toFixed(1) + ' ' +unitsOut
    }
}