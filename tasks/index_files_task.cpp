#include "index_files_task.h"

#include "file-repository/file_repository.h"

#include <QDateTime>
#include <QDir>

#include <QDebug>

#include <QFile>
#include <QThread>
#include <QXmlStreamWriter>

void IndexFilesTask::internalRun() {
	qDebug() << "IndexFilesTask: started";

	_repo = new FileRepository();

	QFile indexFile;
	indexFile.setFileName("index.xml");
	if (!indexFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
		qDebug("IndexFilesTask: file open failed");
		return;
	}

	_xmlWriter = new QXmlStreamWriter(&indexFile);
	_xmlWriter->setAutoFormatting(true);
	_xmlWriter->writeStartDocument();
	_xmlWriter->writeStartElement("files");

	for (const auto &d : QDir::drives()) {
		indexDirectory(d.absoluteDir());
		if (_xmlWriter->hasError())
			break;
	}

	_xmlWriter->writeEndElement();
	_xmlWriter->writeEndDocument();

	delete _xmlWriter;

	// remove thread affinity from object
	_repo->moveToThread(nullptr);

	qDebug() << "IndexFilesTask: ended";
}

QString IndexFilesTask::currDir() {
	std::unique_lock<std::mutex> lock(_m);
	return _currentDir;
}

void IndexFilesTask::indexDirectory(const QDir &dir) {
	emit directoryChanged();

	_m.lock();
	_currentDir = dir.canonicalPath();
	_m.unlock();

	auto entries = dir.entryInfoList(QDir::AllEntries | QDir::NoDotAndDotDot | QDir::NoSymLinks);
	for (auto e : entries) {
		if (isCanceled()) {
			return;
		}

		while (isPaused()) {
			QThread::msleep(50);
			if (isCanceled())
				return;
		}

		if (e.isDir()) {
			indexDirectory(QDir(e.canonicalFilePath()));
			continue;
		}

		if (!e.isFile()) {
			continue;
		}

		_repo->addFileRecord(e.baseName(), e.completeSuffix(), e.canonicalPath(), e.birthTime(), e.size());

		_xmlWriter->writeStartElement("file");
		_xmlWriter->writeAttribute("name", e.baseName());
		_xmlWriter->writeAttribute("ext", e.completeSuffix());
		_xmlWriter->writeAttribute("path", e.canonicalPath());
		_xmlWriter->writeAttribute("date", QString::number(e.birthTime().toSecsSinceEpoch()));
		_xmlWriter->writeAttribute("size", QString::number(e.size()));
		_xmlWriter->writeEndElement();

		_filesCount.store(_filesCount.load(std::memory_order_relaxed) + 1, std::memory_order_relaxed);
		emit filesCountChanged();
	}
}
