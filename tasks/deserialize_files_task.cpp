#include "deserialize_files_task.h"

#include "file-repository/file_repository.h"

#include <QDateTime>
#include <QDebug>
#include <QFile>
#include <QXmlStreamReader>

void DeserializeFilesTask::internalRun() {
	qDebug() << "DeserializeFilesTask: started";

	_repo = new FileRepository();

	QFile indexFile;
	indexFile.setFileName("index.xml");
	if (!indexFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
		qDebug("DeserializeFilesTask: file open failed");
		return;
	}

	QXmlStreamReader xmlReader(&indexFile);

	while (!xmlReader.atEnd()) {
		auto type = xmlReader.readNext();
		if (type == QXmlStreamReader::StartElement && xmlReader.name() == "file") {
			auto attrs = xmlReader.attributes();

			_repo->addFileRecord(
			    attrs.value("name").toString(),
			    attrs.value("ext").toString(),
			    attrs.value("path").toString(),
			    QDateTime::fromSecsSinceEpoch(attrs.value("date").toLongLong()),
			    attrs.value("size").toLongLong());

			_filesCount.store(filesCount() + 1, std::memory_order_relaxed);
		}
	}

	// remove thread affinity from object
	_repo->moveToThread(nullptr);

	qDebug() << "DeserializeFilesTask: ended";
}
