#ifndef SEARCH_FILES_TASK_H
#define SEARCH_FILES_TASK_H

#include "task.h"

#include "file-repository/file_repository.h"

#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QThread>

class SearchFilesTask : public Task {
	Q_OBJECT
public:
	SearchFilesTask(QString property_, QString propertyValue_, FileRepository *repository, QObject *parent = nullptr)
	    : Task(parent), property(property_), propertyValue(propertyValue_), _repo(repository) {}

	QObjectList takeResults() { return std::move(_results); }

private:
	void internalRun() override {
		qDebug() << "SearchFilesTask:started" << property << propertyValue;
		if (!_repo)
			return;

		FileRecordsList list;

		if (property == "name") {
			list = _repo->filesByName(propertyValue);
		} else if (property == "extension") {
			list = _repo->filesByExtension(propertyValue);
		} else if (property == "date") {
			qDebug() << QDateTime::fromMSecsSinceEpoch(propertyValue.toLongLong());
			list = _repo->filesByDate(QDateTime::fromMSecsSinceEpoch(propertyValue.toLongLong()));
		} else if (property == "size") {
			list = _repo->filesBySize(propertyValue.toLongLong());
		}

		if (!list.empty()) {
			qDebug() << "SearchFilesTask: found" << list.length();
			for (auto record : list) {
				_results.push_back(record);
			}
		} else {
			qDebug() << "SearchFilesTask: search in fs";
			// Если нечего не нашли ищем в файловой системе
			for (const auto &d : QDir::drives()) {
				searchDirectory(d.absoluteDir());
				if (_found)
					break;
			}
		}
		qDebug() << "SearchFilesTask:finished";
	}

	void searchDirectory(const QDir &dir) {
		auto entries = dir.entryInfoList(QDir::AllEntries | QDir::NoDotAndDotDot | QDir::NoSymLinks);
		for (auto e : entries) {
			if (isCanceled()) {
				return;
			}

			while (isPaused()) {
				QThread::msleep(50);
				if (isCanceled())
					return;
			}

			if (e.isDir()) {
				searchDirectory(QDir(e.canonicalFilePath()));
				continue;
			}

			if (!e.isFile()) {
				continue;
			}

			if (property == "name" && e.baseName() == propertyValue) {
				_found = true;
			} else if (property == "ext" && e.completeSuffix() == propertyValue) {
				_found = true;
			} else if (property == "date" && e.birthTime().toSecsSinceEpoch() == propertyValue.toLongLong()) {
				_found = true;
			} else if (property == "size" && e.size() == propertyValue.toLongLong()) {
				_found = true;
			}

			if (_found) {
				_results.push_back(_repo->addFileRecord(e.baseName(), e.completeSuffix(), e.canonicalPath(), e.birthTime(), e.size()));
				return;
			}
		}
	}

private:
	QString property;
	QString propertyValue;
	bool _found = false;
	QObjectList _results;
	FileRepository *_repo;
};

#endif // SEARCH_FILES_TASK_H