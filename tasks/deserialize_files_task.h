#ifndef DESERIALIZE_FILES_TASK_H
#define DESERIALIZE_FILES_TASK_H

#include "task.h"

class FileRepository;

class DeserializeFilesTask : public Task {
	Q_OBJECT
	Q_PROPERTY(quint64 filesCount READ filesCount NOTIFY filesCountChanged)
public:
	using Task::Task;

	FileRepository *takeRepo() { return _repo; }

	quint64 filesCount() { return _filesCount.load(std::memory_order_relaxed); }

signals:
	void filesCountChanged();

private:
	void internalRun() override;

private:
	FileRepository *_repo = nullptr;
	std::atomic<quint64> _filesCount{0};
};

#endif // DESERIALIZE_FILES_TASK_H