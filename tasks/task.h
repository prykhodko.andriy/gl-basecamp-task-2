#ifndef TASK_H
#define TASK_H

#include <QObject>
#include <QRunnable>

#include <atomic>

class Task : public QObject, public QRunnable {
	Q_OBJECT
	Q_PROPERTY(bool paused READ isPaused WRITE setPaused NOTIFY pausedChanged)
public:
	Task(QObject *parent);

	uint getProgress();

	bool isCanceled() const;
	bool isPaused() const;

	void setPaused(bool);

	Q_INVOKABLE void cancel();

	void run() override;

signals:
	void pausedChanged();

	void started();
	void finished();

	void progressChanged(uint); // 0 - 100

protected:
	virtual void internalRun() = 0;

	void setProgress(quint64 v);
	void setProgressRange(quint64 min, quint64 max);

private:
	std::atomic_uint _progress;
	std::atomic_bool _paused;
	std::atomic_bool _canceled;
	quint64 _minProgress;
	quint64 _maxProgress;
};

#endif // TASK_H
