#ifndef INDEX_FILES_TASK_H
#define INDEX_FILES_TASK_H

#include "task.h"

#include <mutex>

class QDir;
class QXmlStreamWriter;

class FileRepository;

class IndexFilesTask : public Task {
	Q_OBJECT
	Q_PROPERTY(quint64 filesCount READ filesCount NOTIFY filesCountChanged)
	Q_PROPERTY(QString currDir READ currDir NOTIFY directoryChanged)
public:
	using Task::Task;

	FileRepository *takeRepo() { return _repo; }

	quint64 filesCount() { return _filesCount.load(std::memory_order_relaxed); }
	QString currDir();

signals:
	void directoryChanged();
	void filesCountChanged();
	void fileFound(QString name, QString ext, QString path, QDateTime date, quint64 size);

private:
	void internalRun() override;
	void indexDirectory(const QDir &dir);

private:
	std::atomic<quint64> _filesCount{0};
	QXmlStreamWriter *_xmlWriter = nullptr;
	FileRepository *_repo = nullptr;

	std::mutex _m;
	QString _currentDir;
};

#endif // INDEX_FILES_TASK_H
