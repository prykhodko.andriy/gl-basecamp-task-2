#include "task.h"

#include <QDebug>

template<typename T>
inline T CLIP(T v, T amin, T amax) {
	assert(amin <= amax);
	if (v < amin)
		return amin;
	else if (v > amax)
		return amax;
	return v;
}

Task::Task(QObject *parent)
    : QObject(parent), _minProgress(0), _maxProgress(100) {}

void Task::cancel() {
	qDebug() << "Task::cancel";
	_canceled.store(true, std::memory_order_relaxed);
}

void Task::setPaused(bool v) {
	qDebug() << "Task::setPaused" << v;
	_paused.store(v, std::memory_order_relaxed);
	emit pausedChanged();
}

void Task::run() {
	emit started();
	internalRun();
	emit finished();
}

bool Task::isCanceled() const {
	return _canceled.load(std::memory_order_relaxed);
}

bool Task::isPaused() const {
	return _paused.load(std::memory_order_relaxed);
}

void Task::setProgress(quint64 v) {
	v = CLIP(v, _minProgress, _maxProgress);
	v = (100ULL * (v - _minProgress)) / (_maxProgress - _minProgress);
	if (v != _progress.load(std::memory_order_relaxed)) {
		_progress.store(v, std::memory_order_relaxed);
		emit progressChanged(v);
	}
}

void Task::setProgressRange(quint64 min, quint64 max) {
	assert(min <= max);
	_minProgress = min;
	_maxProgress = max;
	emit progressChanged(0);
}

uint Task::getProgress() {
	return _progress.load(std::memory_order_relaxed);
}
