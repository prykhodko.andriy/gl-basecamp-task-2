#include "backend.h"

#include <QQmlEngine>
#include <QThreadPool>

#include "tasks/deserialize_files_task.h"
#include "tasks/index_files_task.h"
#include "tasks/search_files_task.h"

Backend::~Backend() {
	if (_task) {
		_task->cancel();
		QThreadPool::globalInstance()->waitForDone();
	}
}

void Backend::startIndexingTask() {
	_task = new IndexFilesTask(this);
	_task->setAutoDelete(false);

	// thread-safe connection
	connect(
	    _task, &Task::finished, this, [this] {
		    setRepo(((IndexFilesTask *)_task)->takeRepo());
		    setCurrentTask(nullptr, "idle");
	    },
	    Qt::QueuedConnection);
	setCurrentTask(_task, "indexing");
	QThreadPool::globalInstance()->start(_task);
}

void Backend::startDeserializingTask() {
	qDebug() << "Deserialize task";
	_task = new DeserializeFilesTask(this);
	_task->setAutoDelete(false);

	connect(
	    _task, &Task::finished, this, [this] {
		    setRepo(((DeserializeFilesTask *)_task)->takeRepo());
		    setCurrentTask(nullptr, "idle");
	    },
	    Qt::QueuedConnection);
	setCurrentTask(_task, "loading");
	QThreadPool::globalInstance()->start(_task);
}

void Backend::startSearchTask(QString property, QString propertyValue) {
	_task = new SearchFilesTask(property, propertyValue, _repository, this);
	_task->setAutoDelete(false);
	connect(
	    _task, &SearchFilesTask::finished, this, [this] {
		    _searchResults = ((SearchFilesTask *)_task)->takeResults();
		    emit searchResultsChanged();
		    setCurrentTask(nullptr, "idle");
	    },
	    Qt::QueuedConnection);
	setCurrentTask(_task, "searching");
	QThreadPool::globalInstance()->start(_task);
}

void Backend::setCurrentTask(Task *task, QString type) {
	_taskType = type;
	_task = task;
	emit taskChanged();
}

void Backend::setRepo(FileRepository *repo) {
	_repository = repo;
	_repository->moveToThread(QThread::currentThread());
	_repository->setParent(this);
	emit repositoryChanged();
}
