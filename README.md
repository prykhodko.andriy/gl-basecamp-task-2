# GL BaseCamp Task 2

## Application should:

- Index all files on all local disks.

- find all files and store their name/date/size/path to local storage (XML file).

- Non-blocking UI – it should not freeze while indexing files.
- UI should show directory it currently index.
- GUI controls for start/pause/stop indexing, for searching file.
- Find file in index:

  1. By name
  2. By extension
  3. By date
  4. By size

- If file is not in index, search in file system.

- Compile for Linux and Windows.

- Documentation: requirements specification, software design documentation (application modules description, UML diagrams)

## Compiling

TODO

## Usage:

1. Click on start button to start indexing filesystems
2. After indexing completes, choose search filter and enter search value. Then click Search button.
